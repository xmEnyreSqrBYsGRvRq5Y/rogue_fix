#include "utils.h"

template<> bool HookFunction(void **target,void *hook)
{
	return Mhook_SetHook(target,hook) != FALSE;
}

template<> bool HookFunctionInit(void **target,void *orig,void *hook)
{
	*target = orig;
	return HookFunction(target,hook);
}

template<> bool GetDLLFunction(void **target,HMODULE module,char *name)
{
	*target = GetProcAddress(module,name);
	return *target != 0;
}

template<> bool HookDLLFunction(void **target,HMODULE module,char *name,void *hook)
{
	if (!GetDLLFunction(target,module,name) || !Mhook_SetHook(target,hook))
		*target = 0;
	return *target != 0;
}

template<> bool HookCOM(void **target,IUnknown *obj,int vtableOffs,void *hook)
{
	PVOID **vtblPtr = (PVOID **) obj;
	PVOID *vtbl = *vtblPtr;
	*target = vtbl[vtableOffs];
	return Mhook_SetHook(target,hook) != FALSE;
}

template<> bool UnhookFunction(void **target)
{
	return Mhook_Unhook(target) != FALSE;
}

