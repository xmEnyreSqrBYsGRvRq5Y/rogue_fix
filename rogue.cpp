#include "rogue.h"

#include <ddraw.h>
#include "d3d.h"
#include <MMSystem.h>
#pragma comment (lib,"winmm.lib")
#pragma comment (lib,"dxguid.lib")

#include "utils.h"
#include <string>
#include <windows.h>
using namespace std;

typedef HRESULT (__stdcall *PDirectDrawCreate)(GUID *lpGUID,LPDIRECTDRAW *lplpDD,IUnknown *pUnkOuter);
typedef HRESULT (__stdcall *PDirectDrawCreateEx)(GUID *lpGUID,LPVOID *lplpDD,REFIID iid,IUnknown *pUnkOuter);
static PDirectDrawCreate Real_DirectDrawCreate = 0;
static PDirectDrawCreateEx Real_DirectDrawCreateEx = 0;

typedef HRESULT (__stdcall *PQueryInterface)(IUnknown *dd,REFIID iid,LPVOID *ppObj);
static PQueryInterface Real_DDraw_QueryInterface = 0;
static PQueryInterface Real_DDraw4_QueryInterface = 0;
static PQueryInterface Real_D3DDev_QueryInterface = 0;

typedef HRESULT (__stdcall *PEndScene)(IUnknown *dd);
static PEndScene Real_D3D_DeviceEndScene = 0;

typedef HRESULT (__stdcall *PCreateDevice)(IUnknown *dd,REFIID iid,LPDIRECTDRAWSURFACE4 lpDDS, LPDIRECT3DDEVICE3 *lplpD3DDevice3,LPVOID* lpUnk);
static PCreateDevice Real_D3D_CreateDevice = 0;

static void PatchDDrawInterface(IUnknown *dd,int version);
static HRESULT DDrawQueryInterface(HRESULT hr,REFIID iid,LPVOID *ppObject);

extern "C"  __declspec(dllexport) void export1(void)
{
	MessageBoxA(GetDesktopWindow(),"You are hearing me talk :|","MEOW",MB_ICONINFORMATION);
}

 static HRESULT __stdcall Mine_D3D_DeviceEndScene(IUnknown *dd)
{	
	static DWORD lasttime = 0;
	DWORD   time = timeGetTime();
	unsigned    frameDuration   = 1000 / 40;
	while( (time-lasttime) < frameDuration ) {
		Sleep( 0 );
		time = timeGetTime();
	}
	lasttime = time;
 return Real_D3D_DeviceEndScene(dd);
}

static HRESULT __stdcall Mine_D3D_CreateDevice(IUnknown *dd,REFIID iid,LPDIRECTDRAWSURFACE4 lpDDS, LPDIRECT3DDEVICE3 *lplpD3DDevice3,LPVOID* lpUnk)
{
	HRESULT hr =  Real_D3D_CreateDevice(dd,iid,lpDDS,lplpD3DDevice3,lpUnk);
	if(SUCCEEDED(hr))
	{
		HookCOMOnce(&Real_D3D_DeviceEndScene,*lplpD3DDevice3, 9, Mine_D3D_DeviceEndScene); 
	}
	return hr;
}

static HRESULT __stdcall Mine_DDraw_QueryInterface(IUnknown *dd,REFIID iid,LPVOID *ppObj)
{
	return DDrawQueryInterface(Real_DDraw_QueryInterface(dd,iid,ppObj),iid,ppObj);
}

static HRESULT __stdcall Mine_DDraw4_QueryInterface(IUnknown *dd,REFIID iid,LPVOID *ppObj)
{
	return DDrawQueryInterface(Real_DDraw_QueryInterface(dd,iid,ppObj),iid,ppObj);
}

static void PatchDDrawInterface(IUnknown *dd,int version)
{
#define DDRAW_HOOKS(ver) \
	HookCOMOnce(&Real_DDraw ## ver ## QueryInterface, dd, 0, Mine_DDraw ## ver ## QueryInterface); 

	switch(version)
	{
	case 1: DDRAW_HOOKS(_); break;
	case 4: DDRAW_HOOKS(4_); break;
	}
#undef DDRAW_HOOKS
}

static HRESULT DDrawQueryInterface(HRESULT hr,REFIID iid,LPVOID *ppObject)
{
	if(FAILED(hr) || !ppObject || !*ppObject)
		return hr;

	IUnknown *iface = (IUnknown *) *ppObject;
	if(iid == IID_IDirectDraw)
	{
		PatchDDrawInterface(iface,1);
		//MessageBoxA(GetDesktopWindow(),"DirectDraw 1 :|","MEOW",MB_ICONINFORMATION);
	}
	
	if(iid == IID_IDirectDraw4)
	{
		PatchDDrawInterface(iface,4);
		//MessageBoxA(GetDesktopWindow(),"DirectDraw 4 :|","MEOW",MB_ICONINFORMATION);
	}

	if(iid == IID_IDirect3D3)
	{
		HookCOMOnce(&Real_D3D_CreateDevice, iface, 8, Mine_D3D_CreateDevice); 
		//MessageBoxA(GetDesktopWindow(),"Direct3D3 :|","MEOW",MB_ICONINFORMATION);
	}
	return hr;
}

HRESULT __stdcall Mine_DirectDrawCreate(GUID *lpGUID,LPDIRECTDRAW *lplpDD,IUnknown *pUnkOuter)
{
	HRESULT hr = Real_DirectDrawCreate(lpGUID,lplpDD,pUnkOuter);
	PatchDDrawInterface(*lplpDD,1);
	return hr;
}


HRESULT __stdcall Mine_DirectDrawCreateEx(GUID *lpGUID,LPVOID *lplpDD,REFIID iid,IUnknown *pUnkOuter)
{
	HRESULT hr = Real_DirectDrawCreateEx(lpGUID,lplpDD,iid,pUnkOuter);
	DDrawQueryInterface(hr,iid,lplpDD);
	return hr;
}

void init()
{
	HMODULE ddraw = LoadLibraryA("ddraw.dll");
	if (!ddraw)
		return;
	Real_DirectDrawCreate = (PDirectDrawCreate)GetProcAddress(ddraw, "DirectDrawCreate");
	if (Real_DirectDrawCreate)
		HookFunction(&Real_DirectDrawCreate,Mine_DirectDrawCreate);
	Real_DirectDrawCreateEx = (PDirectDrawCreateEx)GetProcAddress(ddraw, "DirectDrawCreateEx");
	if (Real_DirectDrawCreateEx)
		HookFunction(&Real_DirectDrawCreateEx,Mine_DirectDrawCreateEx);
}

BOOL APIENTRY DllMain(HINSTANCE hModule, DWORD dwReason, PVOID lpReserved)
{
	if(dwReason == DLL_PROCESS_ATTACH)
	{
		init();
	}

	return TRUE;
}